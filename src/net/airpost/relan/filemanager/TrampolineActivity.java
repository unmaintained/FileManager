package net.airpost.relan.filemanager;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

public class TrampolineActivity extends Activity {

    private static final String ACTION_BROWSE_DOCUMENT_ROOT =
            "android.provider.action.BROWSE_DOCUMENT_ROOT";
    private static final String ACTION_BROWSE =
            "android.provider.action.BROWSE";
    private static final String DOCUMENT_AUTHORITY =
            "com.android.externalstorage.documents";
    public static final String EXTRA_SHOW_ADVANCED =
            "android.content.extra.SHOW_ADVANCED";
    public static final String EXTRA_FANCY_FEATURES =
            "android.content.extra.FANCY";
    public static final String EXTRA_SHOW_FILESIZE =
            "android.content.extra.SHOW_FILESIZE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(getBrowseActionString());
        intent.setData(new Uri.Builder()
                .scheme(ContentResolver.SCHEME_CONTENT)
                .authority(DOCUMENT_AUTHORITY)
                .appendPath("root").appendPath("primary")
                .build());
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.putExtra(EXTRA_SHOW_ADVANCED, true);
        intent.putExtra(EXTRA_FANCY_FEATURES, true);
        intent.putExtra(EXTRA_SHOW_FILESIZE, true);
        startActivity(intent);
        finish();
    }

    private static String getBrowseActionString() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            return ACTION_BROWSE_DOCUMENT_ROOT;
        } else {
            return ACTION_BROWSE;
        }
    }

}
